const pkg = require('./package')

import axios from 'axios'


module.exports = {
  mode: 'universal',

  generate: {
    routes: require('./extra_static_routes/extra_routes.json'),
/*
    routes: function () {
      return axios.get('http://localhost:64258/umbraco/api/ProductsApi/GetProductIds')
      .then((res) => {
        return res.data.map((product) => {
          console.log("HIEREHIEIHIEIHEIHEIHEIHIEI" + product.Id);
          return '/products/' + product.Id
        })
      })
    } */
    subFolders: false
  },

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  axios: {
    proxyHeaders: false,
    credentials: false
  },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */

    extend(config, ctx) {
      
    },
  }
}

