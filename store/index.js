import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      products: [],
      product: {}
    }),
    mutations: {
      add (state, payload) {
        state.products = payload;
      },
      setProduct(state, payload){
          state.product = payload
      }
    }
  })
}

export default createStore